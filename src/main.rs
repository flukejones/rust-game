/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#![allow(clippy::pedantic)]
#![allow(clippy::new_without_default)]
#![doc(html_root_url = "http://lukenukem.co.nz/rs_game_framework/")]

use std::time::Instant;

pub use particle2d;
pub use particle2d::vec2d;
use sdl2;
pub use tiny_ecs;

///
/// The manager module contains most I/O objects and interfaces
pub mod manager;

/// The `states` module generally contains 3 states; `menu`, `play`, and `pause`
///
/// * `menu` is what a player will see upon loading the game
/// * `play` is the state which manages all gamplay, including loading assets,
///   initializing objects, managing responses to input, updaing the required
///   objects, and any other related tasks
/// * `pause` is a state which the player should see upon *pausing* a game,
///   it could just display an overlay over the top of the main game state,
///   or it could be more complex with a mini-game, menus, anything.
pub mod states;

/// This is where the main game loop and associated functions live
pub mod game;

/// All resource management structs and functions
pub mod resource;

/// The `components` module is where game entity components live
pub mod components;

/// Player object and menu control bindings + input management
pub mod controls;

/// The possible messages between components
pub mod messaging;

type FP = f64;
const MS_PER_UPDATE: FP = 4.0;

#[derive(Debug)]
pub struct TimeStep {
    last_time: Instant,
    delta_time: FP,
    frame_count: u32,
    frame_time: FP,
}

impl TimeStep {
    pub fn new() -> TimeStep {
        TimeStep {
            last_time: Instant::now(),
            delta_time: 0.0,
            frame_count: 0,
            frame_time: 0.0,
        }
    }

    pub fn delta(&mut self) -> FP {
        let current_time = Instant::now();
        let delta = current_time.duration_since(self.last_time).as_micros()
            as FP
            * 0.001;
        self.last_time = current_time;
        self.delta_time = delta;
        delta
    }

    pub fn frame_rate(&mut self) -> Option<u32> {
        self.frame_count += 1;
        self.frame_time += self.delta_time;
        let tmp;
        if self.frame_time >= 1000.0 {
            tmp = self.frame_count;
            self.frame_count = 0;
            self.frame_time = 0.0;
            return Some(tmp);
        }
        None
    }
}

/// Game initialisation and main loop
pub fn main() {
    // An SDL context is needed before we can procceed
    let mut sdl_ctx = sdl2::init().unwrap();
    // TODO: initialize a settings store here, then load settings
    let mut game = game::Game::new(&mut sdl_ctx, 800, 600);

    let mut timestep = TimeStep::new();
    let mut lag = 0.0;
    'running: loop {
        if !game.running() {
            break 'running;
        }
        game.handle_events();

        lag += timestep.delta();

        while lag >= MS_PER_UPDATE {
            game.update(MS_PER_UPDATE * 0.01);
            lag -= MS_PER_UPDATE;
        }

        game.render(lag);
    }
}
