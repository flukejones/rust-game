/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use crate::messaging::Message;
use sdl2::event::Event;
use sdl2::keyboard::Scancode as Sc;
use sdl2::mouse::MouseButton as Mb;
use sdl2::EventPump;

use std::collections::hash_set::HashSet;
use std::collections::HashMap;

/// A generic version of a `callback`
///
/// # Example
/// ```rust
/// #[macro_use(callback)]
/// # extern crate framework;
/// trait Command {
///     fn run(&self) {}
/// }
/// struct Test { x: i32 }
/// impl Test {
///     fn new() -> Test { Test{ x: 42} }
/// }
/// impl Command for Test {
///     fn run(&self) { println!("{:?}", self.x); }
/// }
/// fn main() {
///     let mut t = Test::new();
///     let macro_test = callback!(Command::run);
///     println!("{:?}", macro_test(&mut t as &mut Command) )
/// }
/// ```
#[macro_export]
macro_rules! callback {
    ($t:ident :: $m:ident) => {{
        fn __f(x: &mut $t) {
            x.$m()
        }
        __f
    }};
}

/// Stores a set of input bindings. Currently used in InputTypes component,
/// can also be used in game states, menus, etc.
pub struct Bindings {
    keys: HashMap<Sc, Message>,
    mouse: HashMap<Mb, Message>,
}

impl Bindings {
    /// Manages all key/controller bindings and their function pointers
    pub fn new() -> Bindings {
        Bindings {
            keys: HashMap::new(),
            mouse: HashMap::new(),
        }
    }

    pub fn init_player(&mut self) {
        self.add_key_bind(Sc::Left, Message::MoveLeft);
        self.add_key_bind(Sc::Up, Message::MoveUp);
        self.add_key_bind(Sc::Right, Message::MoveRight);
        self.add_key_bind(Sc::Down, Message::MoveDown);
        self.add_key_bind(Sc::LCtrl, Message::Shoot);
        self.add_key_bind(Sc::Space, Message::Jump);
    }

    /// Adds a keyboard button binding and action String to the binding manager
    ///
    /// # Example
    /// ```rust
    /// # extern crate sdl2;
    /// #[macro_use(callback)]
    /// # extern crate framework;
    /// # use framework::manager::bindings::*;
    /// # use framework::components::messages::*;
    /// # use sdl2::keyboard::Scancode as Sc;
    /// # fn main() {
    ///     let mut bindings = Bindings::new();
    ///     bindings.add_key_bind(Sc::Left, Message::Shoot);
    /// # }
    /// ```
    pub fn add_key_bind(&mut self, sc: Sc, action: Message) {
        self.keys.insert(sc, action);
    }
    pub fn add_mouse_bind(&mut self, mb: Mb, action: Message) {
        self.mouse.insert(mb, action);
    }

    /// Returns a `PlayerInput` trait wrapped in a function
    ///
    /// This function can then have an object of `PlayerInput` type passed to it for which the
    /// encapsulated command (callback) will be run.
    ///
    /// ```rust
    /// # extern crate sdl2;
    /// #[macro_use(callback)]
    /// # extern crate framework;
    /// # use framework::manager::bindings::*;
    /// # use framework::manager::input::*;
    /// # use framework::components::messages::*;
    /// # use sdl2::keyboard::Scancode as Sc;
    /// # use sdl2::mouse::MouseButton as Mb;
    /// # fn main() {
    ///
    ///     let mut sdl_ctx = sdl2::init().unwrap();
    ///     let input = Input::new(sdl_ctx.event_pump().unwrap());
    ///
    ///     let mut bindings = Bindings::new();
    ///     bindings.add_key_bind(Sc::Left, Message::Player(PlayerMsg::Shoot));
    ///
    ///     for (key, message) in bindings.get_key_binds() {
    ///         if input.get_key(&key) {
    ///             println!("key pressed");
    ///         }
    ///     }
    ///     for (mbtn, message) in bindings.get_mouse_binds() {
    ///         if input.get_mbtn(&mbtn) {
    ///             println!("button pressed");
    ///         }
    ///     }
    /// }
    /// ```
    pub fn get_key_bind(&self, sc: Sc) -> Option<&Message> {
        self.keys.get(&sc)
    }
    pub fn get_mouse_bind(&self, mb: Mb) -> Option<&Message> {
        self.mouse.get(&mb)
    }

    ///
    /// Returns a `&HashMap` containing all `PlayerInput` physics_funcs
    ///
    pub fn get_key_binds(&self) -> &HashMap<Sc, Message> {
        &self.keys
    }
    pub fn get_mouse_binds(&self) -> &HashMap<Mb, Message> {
        &self.mouse
    }
}

/// Fetch all input
pub struct Input {
    pump: EventPump,
    key_state: HashSet<Sc>,
    mouse_state: HashSet<Mb>,
    mouse_pos: (i32, i32),
    quit: bool,
}

impl Input {
    pub fn new(pump: EventPump) -> Input {
        Input {
            pump,
            key_state: HashSet::new(),
            mouse_state: HashSet::new(),
            mouse_pos: (0, 0),
            quit: false,
        }
    }

    /// The way this is set up to work is that for each `game tick`, a fresh set of event is
    /// gathered and stored. Then for that single game tick, every part of the game can ask
    /// `Input` for results without the results being removed.
    ///
    /// The results of the `update` are valid until the next `update` whereupon they are refreshed.
    ///
    /// **rust-sdl2** provides an `event_iter()`, but this isn't very useful unless we perform
    /// all the required actions in the same block that it is called in. It has the potential
    /// to cause delays in proccessing
    ///
    pub fn update(&mut self) {
        if let Some(event) = self.pump.poll_event() {
            match event {
                Event::KeyDown { .. } => {
                    self.key_state = self
                        .pump
                        .keyboard_state()
                        .pressed_scancodes()
                        .collect();
                }
                Event::KeyUp { .. } => {
                    self.key_state = self
                        .pump
                        .keyboard_state()
                        .pressed_scancodes()
                        .collect();
                }

                Event::MouseButtonDown { mouse_btn, .. } => {
                    self.mouse_state.insert(mouse_btn);
                }
                Event::MouseButtonUp { mouse_btn, .. } => {
                    self.mouse_state.remove(&mouse_btn);
                }

                Event::Quit { .. } => self.quit = true, // Early out if Quit

                Event::MouseMotion { x, y, .. } => {
                    self.mouse_pos.0 = x;
                    self.mouse_pos.1 = y;
                }
                _ => {}
            }
        }
    }

    pub fn get_key(&self, s: Sc) -> bool {
        self.key_state.contains(&s)
    }

    pub fn get_mbtn(&self, m: Mb) -> bool {
        self.mouse_state.contains(&m)
    }

    pub fn get_mpos(&self) -> (i32, i32) {
        self.mouse_pos
    }

    pub fn get_quit(&self) -> bool {
        self.quit
    }
}
