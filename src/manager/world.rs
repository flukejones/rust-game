/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use std::path::PathBuf;

use sdl2::rect::Rect;
use tiled_json_rs as tiled;
use tiny_ecs::Entities;

use super::camera::Camera;
use crate::components::{parts, systems};
use crate::controls::Input;
use crate::messaging::{MessageBus, MessageListener};
use crate::particle2d::contact_generators::AreaContact;
use crate::particle2d::world::World as PhysicsWorld;
use crate::resource::{CachedTile, SpriteSheets};
use crate::vec2d::Vec2d;
use crate::FP;
use sdl2::{image::LoadTexture, render::Canvas, video::Window};
use tiled_json_rs::{Layer, TileLayer};

pub struct World {
    level_change: bool,
    player: usize,
    pub entities: Entities,
    pub physics_world: PhysicsWorld,
    /// `active_ents` contains the active *index* number for
    /// lookup in Entities.mask
    active_ents: Vec<usize>,
    map: Vec<Layer>,
    tiles: Vec<CachedTile>,
    pub sprite_sheets: SpriteSheets,
    dimensions: Vec2d<u32>,
    msg_bus: MessageBus, // Messages is the enum of all possible messages
    msg_listeners: Vec<Box<dyn MessageListener>>,
    cam: Camera,
}

/// The game world (ECS) controller
impl World {
    pub fn new() -> World {
        World {
            level_change: false,
            player: 0,
            entities: Default::default(),
            physics_world: PhysicsWorld::new(10000, 500, None),
            active_ents: vec![],
            map: vec![],
            tiles: vec![],
            sprite_sheets: SpriteSheets::new(),
            dimensions: Vec2d::new(0, 0),
            msg_bus: MessageBus::new(),
            msg_listeners: vec![],
            cam: Camera::new(),
        }
    }

    pub fn init(&mut self, map_path: PathBuf, canvas: &mut Canvas<Window>) {
        // Load/parse
        let map = tiled::Map::load_from_file(map_path.as_path())
            .expect("Failed to load map");

        // Add sprite sheets so we don't need to load from disk on access
        let mut sprite_sheets = SpriteSheets::new();
        let texture_creator = canvas.texture_creator();

        let texture = texture_creator
            .load_texture(&PathBuf::from("assets/test.png"))
            .expect("Couldn't load QuakeFace");
        sprite_sheets.push(&"QuakeFace".to_string(), texture);

        let texture = texture_creator
            .load_texture(&PathBuf::from("assets/quake2.png"))
            .expect("Couldn't load QuakeFace");
        sprite_sheets.push(&"quake2.png".to_string(), texture);

        //
        let mut tiles: Vec<CachedTile> = Vec::new();
        let blank = CachedTile::new(Rect::new(0, 0, 0, 0), String::from(""));
        tiles.push(blank);

        for tileset in &map.tile_sets {
            // TODO: force alloc exact space for tilecount (vec)
            let name = tileset.name.clone();
            let mut path = PathBuf::from("assets");
            path.push(tileset.image.clone());

            let texture = texture_creator
                .load_texture(&PathBuf::from(&path))
                .expect(format!("Couldn't load {:?}", &path).as_str());
            sprite_sheets.push(&name, texture);

            for id in 0..=tileset.tile_count {
                let tile_rect = tileset.tile_position_on_image(id);
                let source_rect = sdl2::rect::Rect::new(
                    tile_rect.x,
                    tile_rect.y,
                    tile_rect.width,
                    tile_rect.height,
                );
                tiles.push(CachedTile::new(source_rect, name.clone()));
            }
        }

        // world dimension in pixels
        let pixel_width = map.width * map.tile_width;
        let pixel_height = map.height * map.tile_height;

        let min = Vec2d::new(0.0, 0.0);
        let max = Vec2d::new(pixel_width as FP, pixel_height as FP);
        // Add constraints to physics world
        self.physics_world
            .add_contact_generator(AreaContact::new(min, max));

        self.cam.set(
            canvas.viewport(),
            Rect::new(0, 0, pixel_width, pixel_height),
            Rect::new(0, 0, 400, 300),
        );

        self.level_change = false;
        self.entities = Entities::new(Some(20000), Some(31));

        self.active_ents = Vec::new();
        self.map = map.layers;
        self.tiles = tiles;
        self.sprite_sheets = sprite_sheets;
        self.dimensions = Vec2d::new(pixel_width, pixel_height);
    }

    pub fn set_player(&mut self, player: usize) {
        self.player = player
    }

    // TODO: remove a listener fn
    pub fn add_listener(&mut self, listener: impl MessageListener + 'static) {
        self.msg_listeners.push(Box::new(listener));
    }

    /// Updates all active entities using the appropriate systems
    ///
    /// The required arrays will be passed to a System for it to use, rather than one entity
    /// at a time. Things like collisions will need all valid entities. It is then up to the System
    /// to filter out *illegal* entities (by eg, checking current index is non-zero for all array)
    ///
    /// of the scene-graph.
    pub fn update(&mut self, dt: FP, _input: &Input) {
        // The physics world must be reset
        self.physics_world.start_frame();

        // set viewport position
        if let Ok(particle_map) = self.entities.borrow::<parts::ParticleID>() {
            if let Some(p) = particle_map.get(self.player) {
                let player =
                    self.physics_world.get_particle_position(p.id).unwrap();
                let pos_i32 = Vec2d::new(
                    player.x() as i32,
                    self.dimensions.y() as i32 - player.y() as i32,
                );
                self.cam.update_position(pos_i32);
            };
        };

        self.active_ents.clear();
        self.active_ents.push(self.player);

        // input - entities with input and velocity. Player specific, will have defined set of actions
        // for example, and elevator input might check positions then switch velocity.
        for eid in &self.active_ents {
            systems::input_update(
                *eid as usize,
                &mut self.entities,
                &mut self.msg_bus,
                _input,
            );
            systems::animation_update(*eid, &mut self.entities, dt);
        }

        for listener in &mut self.msg_listeners {
            listener.update(&mut self.msg_bus, &mut self.entities);
        }

        self.physics_world.run_physics(f64::from(dt));
        // interations - player/ai, for eg, damage?? Covered by player_update and ai_update?
    }

    fn render_tile_layer(
        &self,
        tile_layer: &TileLayer,
        surface: &mut Canvas<Window>,
    ) {
        //let screen_pos = self.cam.position();
        for (i, id) in tile_layer.data.iter().enumerate() {
            if *id != 0 {
                let tile_rect = self.tiles[*id as usize].get_src_rect().clone();
                let sprite = self
                    .sprite_sheets
                    .get_ref(self.tiles[*id as usize].get_src_name());
                let pos = tile_layer.tile_position_on_layer(i as u32);
                let x = pos.x * tile_rect.width();
                let y = pos.y * tile_rect.height();
                let screen_coords =
                    self.cam.world_to_screen(Vec2d::new(x as i32, y as i32));
                let dst_rect = Rect::new(
                    screen_coords.x() as i32,
                    screen_coords.y() as i32,
                    tile_rect.width() * self.cam.world_ratio().x() as u32,
                    tile_rect.height() * self.cam.world_ratio().y() as u32,
                );
                // blit the tile image to surface
                surface
                    .copy(sprite, Some(tile_rect), Some(dst_rect))
                    .unwrap();
            }
        }
    }

    fn render_layers(
        &self,
        layers: &Vec<Layer>,
        mut canvas: &mut Canvas<Window>,
    ) {
        for layer in layers {
            match &layer.layer_type {
                tiled::LayerType::TileLayer(tiles) => {
                    self.render_tile_layer(tiles, &mut canvas);
                }
                tiled::LayerType::Group { layers } => {
                    &mut self.render_layers(layers, canvas);
                }
                tiled::LayerType::ImageLayer(image) => {
                    let sprite = self
                        .sprite_sheets
                        .get_ref(image.image.to_str().unwrap());
                    let source_rect = Rect::new(
                        layer.offset_x.abs() as i32,
                        layer.offset_y.abs() as i32,
                        self.dimensions.x(),
                        self.dimensions.y(),
                    );
                    canvas.copy(sprite, source_rect, None).unwrap();
                }
                tiled::LayerType::ObjectGroup(_objects) => {}
            }
        }
    }

    /// The final stage after a world update
    ///
    /// Fetch all textures and positions, then draw what is visible.
    pub fn render(&mut self, _dt: FP, mut display: &mut Canvas<Window>) {
        // render tiles first
        self.render_layers(&self.map, &mut display);

        if let Ok(sprites) = self.entities.borrow::<parts::Sprite>() {
            if let Ok(particles) = self.entities.borrow::<parts::ParticleID>() {
                for (id, sprite) in sprites.iter() {
                    if let Some(physics_id) = particles.get(id) {
                        if let Some(position) = self
                            .physics_world
                            .get_particle_position(physics_id.id)
                        {
                            let mut dst = sprite.frame;
                            let pos_i32 = Vec2d::new(
                                position.x() as i32,
                                self.dimensions.y() as i32
                                    - position.y() as i32,
                            );
                            let screen_coords =
                                self.cam.world_to_screen(pos_i32);
                            dst.set_x(screen_coords.x() as i32);
                            dst.set_y(screen_coords.y() as i32);
                            dst.set_width(
                                dst.width() * self.cam.world_ratio().x() as u32,
                            );
                            dst.set_height(
                                dst.height()
                                    * self.cam.world_ratio().y() as u32,
                            );
                            let texture =
                                self.sprite_sheets.get_ref(&sprite.sid);
                            display.copy(texture, None, Some(dst)).unwrap();
                        }
                    }
                }
            }
        }
    }
}
