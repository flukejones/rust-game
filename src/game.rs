/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::keyboard::Scancode;
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::Sdl;

use crate::controls::{Bindings, Input};
use crate::states::menu::Menu;
use crate::states::pause::Pause;
use crate::states::play::Play;
use crate::states::States;
use crate::FP;

pub struct Game {
    bindings: Bindings,
    input: Input,
    states: States,
    canvas: Canvas<Window>,
    running: bool,
    state_changing: bool,
}

impl Game {
    /// On `Game` object creation, initialize all the game subsystems where possible
    ///
    /// Ideally full error checking will be done in by system.
    ///
    pub fn new(sdl_ctx: &mut Sdl, x: u32, y: u32) -> Game {
        let video_ctx = sdl_ctx.video().unwrap();
        // Create a window
        let window: Window = video_ctx
            .window("Game Framework", x, y)
            .position_centered()
            .opengl()
            .build()
            .unwrap();

        let canvas = window
            .into_canvas()
            .accelerated()
            .present_vsync()
            .build()
            .unwrap();

        let events = sdl_ctx.event_pump().unwrap();

        let mut bindings = Bindings::new();
        bindings.init_player();

        let input = Input::new(events);

        let mut states = States::new();
        states.push(Box::new(Menu::new()));

        Game {
            bindings,
            input,
            states,
            canvas,
            running: true,
            state_changing: false,
        }
    }

    /// Called by the main loop
    pub fn update(&mut self, time: FP) {
        self.states.update(time, &self.input, &mut self.bindings);
        self.running = !self.input.get_quit();
    }

    /// `handle_events` updates the current events and inputs plus changes `states`
    ///
    /// In an C++ engine, using a button to switch states would probably be
    /// handled in the state itself. We can't do that with rust as it requires
    /// passing a mutable reference to the state machine to the state; essentially
    /// this is the same as an object in an Vec<Type> trying to modify its container.
    ///
    /// So because of the above reasons, `states::States` does not allow a game state
    /// to handle state changes or fetching
    ///
    pub fn handle_events(&mut self) {
        self.input.update();

        if self.input.get_key(Scancode::Escape) {
            if self.states.get_state_id().unwrap() == "_MENU"
                && !self.state_changing
            {
                self.states.pop();
                let play = Play::new(&mut self.canvas);
                self.states.change(Box::new(play));
                self.state_changing = true;
            } else if self.states.get_state_id().unwrap() == "_PLAY"
                && !self.state_changing
            {
                self.states.push(Box::new(Pause::new()));
                self.state_changing = true;
            } else if self.states.get_state_id().unwrap() == "_PAUSE"
                && !self.state_changing
            {
                self.states.pop();
                self.state_changing = true;
            }
        } else if self.input.get_key(Scancode::Return) {
            if self.states.get_state_id().unwrap() == "_PAUSE"
                && !self.state_changing
            {
                self.states.pop();
                self.states.change(Box::new(Menu::new()));
                self.state_changing = true;
            }
        } else if !self.input.get_key(Scancode::Escape)
            && !self.input.get_key(Scancode::Return)
        {
            self.state_changing = false;
        }
    }

    /// `render` calls the `states.render()` method with a time-step for state renders
    ///
    /// The main loop, in this case, the `'running : loop` in lib.rs should calculate
    /// a time-step to pass down through the render functions for use in the game states,
    /// from which the game states (or menu) will use to render objects at the correct
    /// point in time.
    ///
    pub fn render(&mut self, dt: FP) {
        // The state machine will handle which state renders to the surface
        self.states.render(dt, &mut self.canvas);
        self.canvas.present();
    }

    /// Called by the main loop
    pub fn running(&self) -> bool {
        self.running
    }
}
