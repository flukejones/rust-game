/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/// The menu state and its related data and logic
pub mod menu;
/// The pause state and its related data and logic
pub mod pause;
/// The play state and its related data and logic
pub mod play;

use crate::controls::{Bindings, Input};
use crate::FP;
use sdl2::pixels::Color as SdlColour;
use sdl2::render::Canvas;
use sdl2::video::Window;
use std::collections::VecDeque;

/// Required traits for proper interaction with each state
pub trait GameState {
    /// General I/O will happen here
    fn update(&mut self, dt: FP, input: &Input, bindings: &mut Bindings);

    /// Rendering is separated from updates so that both can run at different speeds
    fn render(&mut self, dt: FP, canvas: &mut Canvas<Window>);

    /// Called when the state is entered
    ///
    /// This is where any set-up methods should live, such as level loading, object loading
    fn enter(&mut self);

    /// Called on the state below the popped state
    fn resume(&mut self);

    /// Called on the existing state when a new state is pushed on top
    fn pause(&mut self);

    /// Called when the state exits
    fn exit(&mut self);

    /// Returns the state ID, used for checking which state is active
    fn get_id(&self) -> &String;
}

/// The game state machine
pub struct States {
    game_states: VecDeque<Box<dyn GameState>>,
}
impl States {
    pub fn new() -> States {
        States {
            game_states: VecDeque::new(),
        }
    }

    /// Push a new state on to the queue
    pub fn push(&mut self, state: Box<dyn GameState>) {
        // Call pause on the currently running state
        if let Some(state) = self.game_states.back_mut() {
            state.pause()
        }
        self.game_states.push_back(state);
        if let Some(state) = self.game_states.back_mut() {
            state.enter()
        }
    }

    /// Pop the active state from the queue
    pub fn pop(&mut self) {
        if let Some(state) = &mut self.game_states.pop_back() {
            state.exit();
            if let Some(last) = self.game_states.back_mut() {
                last.resume();
            }
        }
    }

    /// Change the active state
    ///
    /// Swaps the active state in the Queue for the one requested
    pub fn change(&mut self, state: Box<dyn GameState>) {
        if let Some(st) = self.game_states.back() {
            if st.get_id() == state.get_id() {
                return;
            }
        }
        if let Some(mut st) = self.game_states.pop_back() {
            st.exit();
        }

        self.game_states.push_back(state);
        self.game_states.back_mut().unwrap().enter();
    }

    /// The `State::update()` the current active state and run its `GameState::update()`
    ///
    /// If you were to have an overlay `pause` state for whcih you can see the `play`
    /// state in the background, you would change this method to update all states in the
    /// queue.
    ///
    pub fn update(
        &mut self, dt: FP, input: &Input, mut bindings: &mut Bindings,
    ) {
        if let Some(st) = self.game_states.back_mut() {
            st.update(dt, input, &mut bindings)
        }
    }

    ///
    /// Used for checking which state is currently active
    ///
    pub fn get_state_id(&self) -> Option<&String> {
        if let Some(st) = self.game_states.back() {
            return Some(st.get_id());
        }
        None
    }

    /// This is where the bulk of rendering work is done
    ///
    /// The main purpose of this method is to pass an appropiate surface down through a state
    /// for which objects can be drawn to. Once the state has finished its work, the surface
    /// is then drawn to the screen.
    ///
    pub fn render(&mut self, dt: FP, canvas: &mut Canvas<Window>) {
        match self.game_states.back_mut() {
            Some(state) => state.render(dt, canvas),
            _ => {
                canvas.set_draw_color(SdlColour::RGB(30, 30, 30));
                canvas.fill_rect(None).unwrap();
            }
        }
    }
}
