/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use crate::{
    components::create,
    controls::{Bindings, Input},
    manager::world::World,
    states::GameState,
    FP,
};
use sdl2::{render::Canvas, video::Window};
use std::path::PathBuf;

pub struct Play {
    id: String,
    world: World,
}

impl Play {
    pub fn new(canvas: &mut Canvas<Window>) -> Play {
        let mut world = World::new();
        world.init(PathBuf::from("assets/test.json"), canvas);
        create::player(&mut world, 0, 0, "QuakeFace").unwrap();
        Play {
            id: "_PLAY".to_string(),
            world,
        }
    }
}

impl GameState for Play {
    fn update(&mut self, dt: FP, input: &Input, _bindings: &mut Bindings) {
        self.world.update(dt, input);
    }

    fn render(&mut self, dt: FP, mut canvas: &mut Canvas<Window>) {
        self.world.render(dt, &mut canvas);
    }

    fn enter(&mut self) {}

    fn resume(&mut self) {}

    fn pause(&mut self) {}

    fn exit(&mut self) {}

    fn get_id(&self) -> &String {
        &self.id
    }
}
